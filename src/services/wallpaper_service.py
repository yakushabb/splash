import os
from shutil import copyfile
from pathlib import Path
from gi.repository import Gio, GLib

import requests

from .unsplash_service import UnSplashImage


class WallPaperService:
    cache_dir = GLib.get_user_cache_dir()
    pictures_dir = GLib.get_user_special_dir(GLib.USER_DIRECTORY_PICTURES)

    splash_wallpaper_folder_path = os.path.join(
        cache_dir,
        "in.bharatkalluri.splash"
    )

    Path(splash_wallpaper_folder_path).mkdir(parents=True, exist_ok=True)
    background_settings = Gio.Settings.new("org.gnome.desktop.background")
    current_wallpaper_metadata: UnSplashImage = None

    def set_wallpaper_from_file_uri(self, file_uri: str):
        self.background_settings['picture-uri'] = f"file:///{file_uri}"

    @staticmethod
    def write_image_url_to_file(image_url: str, file_uri: str):
        response = requests.get(image_url, stream=True)
        if response.ok:
            with open(file_uri, "wb") as wallpaper_path:
                wallpaper_path.write(response.raw.read())

    def get_cache_file_uri(self, unsplash_image_metadata: UnSplashImage) -> str:
        return os.path.join(self.splash_wallpaper_folder_path, unsplash_image_metadata.image_id)

    def set_wallpaper_from_unsplash_image(self, unsplash_image_metadata: UnSplashImage) -> str:
        self.current_wallpaper_metadata = unsplash_image_metadata

        downloaded_image_file_uri = self.get_cache_file_uri(unsplash_image_metadata)
        self.write_image_url_to_file(unsplash_image_metadata.full_url, downloaded_image_file_uri)
        self.set_wallpaper_from_file_uri(downloaded_image_file_uri)
        return downloaded_image_file_uri

    def save_current_wallpaper_in_pictures(self):
        if self.current_wallpaper_metadata is not None:
            copyfile(
                self.get_cache_file_uri(self.current_wallpaper_metadata),
                os.path.join(self.pictures_dir, f"{self.current_wallpaper_metadata.image_id}.jpg")
            )

    instance = None
    @staticmethod
    def get_instance():
        if WallPaperService.instance is None:
            WallPaperService.instance = WallPaperService()
        return WallPaperService.instance

