# window.py
#
# Copyright 2020 Bharat Kalluri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GdkPixbuf, Gio
from ..services.wallpaper_service import WallPaperService
from ..services.unsplash_service import UnSplashService, UnSplashImage
from .header_bar import HeaderBar


@Gtk.Template(resource_path='/in/bharatkalluri/splash/ui/window.ui')
class SplashWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'SplashWindow'

    wallpaper_container: Gtk.Image = Gtk.Template.Child()
    header_bar: Gtk.HeaderBar = HeaderBar.get_instance()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_icon_name('in.bharatkalluri.splash')
        self.set_default_size(1024, 768)
        self.set_titlebar(self.header_bar)

    instance = None
    @staticmethod
    def get_instance(**kwargs):
        if SplashWindow.instance is None:
            SplashWindow.instance = SplashWindow(**kwargs)
        return SplashWindow.instance
