# main.py
#
# Copyright 2020 Bharat Kalluri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio

from .views.window import SplashWindow


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='in.bharatkalluri.splash',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def set_up_actions(self):
        actions = [
            {
                'name': 'about',
                'func': self.show_about_dialog,
            },
        ]

        for a in actions:

            action_name: str = a['name']
            fn_for_action = a['func']
            accel_for_action = a.get('accel')

            c_action = Gio.SimpleAction.new(action_name, None)
            c_action.connect('activate', fn_for_action)
            self.add_action(c_action)
            if 'accel' in a.keys():
                self.set_accels_for_action(
                    f'app.{a["name"]}',
                    [accel_for_action],
                )

    def show_about_dialog(self, *args):
        builder = Gtk.Builder.new_from_resource('/in/bharatkalluri/splash/ui/about_dialog.ui')
        dialog = builder.get_object('about_dialog')
        dialog.set_modal(True)
        dialog.set_transient_for(self.props.active_window)
        dialog.present()


    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = SplashWindow.get_instance(application=self)
        self.set_up_actions()
        win.present()


def main(version):
    app = Application()
    return app.run(sys.argv)
