# Splash

<img src="./data/icons/in.bharatkalluri.splash.svg" width="128px" height="128px" />

<p>A UnSplash Wallpaper application built for GNOME.</p>

## Install

<a href="https://flathub.org/apps/details/in.bharatkalluri.splash">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>


Using Splash, you can set your dekstop background randomly from a huge pool of pictures from unsplash.

If you like a picture, you can download it to your pictures folder as well!

## Screenshot

![screenshot](./screenshot.png)

## Credits

- Inspired by [Splash on windows](https://www.microsoft.com/en-us/p/splash-unsplash-wallpaper/9phttwjgg81f)
